type Tap = {
	<A>(fn: (value: A) => Promise<any>): (value: A) => Promise<A>
	<A>(fn: (value: A) => any): (value: A) => A
}

export const tap: Tap = <A>(fn: (_value: A) => any) =>
	(value: A) => {
		const result = fn(value)
		return result instanceof Promise
			? result.then(() => value)
			: value
	}
