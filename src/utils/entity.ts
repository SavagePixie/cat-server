import type { BaseEntity } from '../lib/entity'

type Change = {
	<A extends BaseEntity>(base: A, update: Partial<A>): A
	<A extends BaseEntity>(update: Partial<A>): (base: A) => A
}

export const change: Change =
	<A extends BaseEntity>(...args: [Partial<A>] | [A, Partial<A>]) => {
		if (args.length === 1) return (base: A) => change(base, args[0])

		const [ base, update ] = args
		Object.entries(update)
			.forEach(([ key, value ]) => {
				if (Object.hasOwn(base, key)) base[key] = value
			})

		return base
	}

