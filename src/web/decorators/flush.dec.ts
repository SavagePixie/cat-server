import { repo } from '../../lib/repo'
import { tap } from '../../utils/function'

export function flush(
	_target: any,
	_name: string,
	descriptor: PropertyDescriptor
) {
	const method = descriptor.value
	descriptor.value = (...args: any[]) =>
		Promise.resolve(method.apply(this, args))
			.then(tap(() => repo.em.flush()))
}
