import { Post } from '../../lib/post/post'

import type { RequestHandler } from 'express'

export const fetchPost: RequestHandler = (req, res, next) => {
	res.locals.fetches = {
		...res.locals.fetches,
		post: Post.getById(req.params.id),
	}
	next()
}
