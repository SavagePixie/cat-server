import { array, either, task, taskOption } from 'fp-ts'
import { pipe } from 'fp-ts/lib/function'

import {
	Logger,
	body, controller, del, fetches,
	get, input, middleware, params,
	post, put, query, status,
	use, validation
} from '../../../@arch'
import { Post, PostEntity } from '../../lib/post'
import { fetchPost } from '../middleware/fetch-post'
import { flush } from '../decorators'
import { schemas } from '../../lib/post'

import type {
	NewPost, PostChanges,
	PostId, PostIds
} from '../../lib/post/schemas'
import type { RequestHandler } from 'express'
import type { TaskOption } from 'fp-ts/lib/TaskOption'

@controller('/posts')
export class PostController {
	@use()
	log(): RequestHandler {
		return (_req, _res, next) => {
			Logger.debug('Handling posts')
			next()
		}
	}

	@get()
	list() {
		return pipe(
			Post.list(),
			task.map(data => ({ data }))
		)()
	}

	@validation('query', schemas.withIds)
	@get('/ids')
	getAllIdsOrNone(@query { ids }: PostIds) {
		return pipe(
			ids,
			array.map(Post.getById),
			taskOption.sequenceArray,
			taskOption.map(data => ({ data }))
		)
	}

	@validation('params', schemas.withId)
	@get('/:id')
	getOne(@params { id }: PostId) {
		return pipe(
			id,
			Post.getById,
			taskOption.map(data => ({ data }))
		)()
	}

	@validation('body', schemas.create)
	@post()
	@flush
	create(@body newPost: NewPost) {
		return Post.create(newPost)
	}

	@validation('input', schemas.update)
	@middleware(fetchPost)
	@put('/:id')
	@flush
	update(
		@input changes: PostChanges,
		@fetches('post') post: TaskOption<PostEntity>
	) {
		return pipe(
			Post.update(post, changes),
			taskOption.map(data => ({ data })),
			task.map(either.fromOption(() => ({ code: 404, message: 'Post not found' })))
		)()
	}

	@status(204)
	@validation('params', schemas.withId)
	@middleware(fetchPost)
	@del('/:id')
	@flush
	delete(@fetches('post') post: TaskOption<PostEntity>) {
		return Post.remove(post)()
	}
}
