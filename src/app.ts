import { MikroORM, RequestContext } from '@mikro-orm/core'
import { TsMorphMetadataProvider } from '@mikro-orm/reflection'
import { defineConfig } from '@mikro-orm/postgresql'
import express from 'express'
import pino from 'pino-http'

import { Logger, catchError, methodNotAllowed, router } from '../@arch'
import { PostController } from './web/controllers/post.ctr'
import { PostEntity } from './lib/post'
import { config } from '../config'
import { setRepo } from './lib/repo'

const port = config.getOrThrow('port')

const app = express()

const mikroConfig = defineConfig({
	...config.getOrThrow('database'),
	metadataProvider: TsMorphMetadataProvider,
	entities: [ './dist/src/**/*.entity.js' ],
	entitiesTs: [ './src/**/*.entity.js' ],
	schemaGenerator: {
		createForeignKeyConstraints: true,
		disableForeignKeys: false,
	},
})

MikroORM.init(mikroConfig)
	// .then(mikro => mikro
	// 	.getSchemaGenerator()
	// 	.createSchema()
	// 	.then(() => mikro)
	// )
	.then(mikro => {
		setRepo({
			em: mikro.em,
			post: mikro.em.getRepository(PostEntity),
		})

		app.use(pino({ logger: Logger }))
		app.use(express.json())
		app.use((_req, _res, next) => RequestContext.create(mikro.em, next))

		app.get('/ping', (_req, res) => res.send('pong'))

		router(app, PostController, [])

		app.use(methodNotAllowed, catchError)

		app.listen(port, () => Logger.info(`Server listening on port ${port}`))
	})
