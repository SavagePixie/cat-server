import { randomUUID } from 'node:crypto'

import { PrimaryKey, Property } from '@mikro-orm/core'

export abstract class BaseEntity {
	@PrimaryKey({ type: 'uuid' })
	id: string = randomUUID()

	@Property()
	createdAt: Date = new Date()

	@Property({ onUpdate: () => new Date() })
	updatedAt: Date = new Date()
}
