import { Type } from '@mikro-orm/core'
import { option } from 'fp-ts'

import type { Option } from 'fp-ts/lib/Option'

export class EntityOption<A> extends Type<Option<A>, A> {
	convertToDatabaseValue(value: Option<A>): A | null {
		return this.toJSON(value)
	}

	convertToJSValue(value: A): Option<A> {
		return option.fromNullable(value)
	}

	toJSON(value: Option<A>): A | null {
		return option.getOrElse(() => null)(value)
	}
}
