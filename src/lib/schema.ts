import { P, match } from 'ts-pattern'
import { either, option } from 'fp-ts'
import { pipe } from 'fp-ts/lib/function'
import { z } from 'zod'

const optional = <A extends z.ZodTypeAny>(schema: A) => schema
	.nullable()
	.optional()
	.transform(option.fromNullable)

export const s = { optional }

export const validate = (schema: z.ZodTypeAny) =>
	(input: unknown) =>
		pipe(
			schema.safeParse(input),
			value => match(value)
				.with({ success: true, data: P.select() }, either.right)
				.with(
					{ success: false, error: P.select() },
					({ issues }) => either.left(issues)
				)
				.exhaustive()
		)
