import { P } from 'ts-pattern'

export const M = {
	None: { _tag: 'None' } as const,
	Some: { _tag: 'Some', value: P.any } as const,
	Left: { _tag: 'Left', left: P.any } as const,
	Right: { _tag: 'Right', right: P.any } as const,
}
