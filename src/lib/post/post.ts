import { option, task, taskOption } from 'fp-ts'
import { pipe } from 'fp-ts/lib/function'

import { Repo, repo } from '../repo'
import { change } from '../../utils/entity'
import { tap } from '../../utils/function'

import type { NewPost, PostChanges } from './schemas'
import type { PostEntity } from './post.entity'
import type { TaskOption } from 'fp-ts/lib/TaskOption'

export const postFactory = (repo: Repo) => {
	const create = (row: NewPost) => pipe(
		repo.post.create(row),
		tap(post => repo.em.persist(post))
	)

	const getById = (id: string) => pipe(
		{ id },
		opts => () => repo.post.findOne(opts),
		task.map(option.fromNullable)
	)

	const list = () => task.of(repo.post.findAll())

	const remove = (post: TaskOption<PostEntity>) => pipe(
		post,
		taskOption.map(tap(post => repo.em.remove(post)))
	)

	const update = (post: TaskOption<PostEntity>, changes: PostChanges) => pipe(
		post,
		taskOption.map(change(changes)),
		taskOption.map(tap(post => repo.em.persist(post)))
	)

	return {
		create,
		getById,
		list,
		remove,
		update,
	}
}

export const Post = postFactory(repo)
