import { z } from 'zod'

import { s } from '../schema'

export type NewPost = z.infer<typeof create>
export type PostChanges = z.infer<typeof update>
export type PostId = z.infer<typeof withId>
export type PostIds = z.infer<typeof withIds>

export const create = z.object({
	image: s.optional(z.string()),
	summary: z.string()
		.max(500)
		.min(15),
	text: z.string()
		.max(5000)
		.min(15),
	title: z.string()
		.max(120)
		.min(5),
})

export const update = create.extend({ id: z.string().uuid() })

export const withId = z.object({ id: z.string().uuid() })

export const withIds = z.object({ ids: z.array(z.string().uuid()) })
