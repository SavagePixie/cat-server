import * as _schemas from './schemas'

export { PostEntity } from './post.entity'
export { Post, postFactory } from './post'

export const schemas = _schemas
