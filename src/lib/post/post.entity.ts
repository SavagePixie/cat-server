import { Entity, Property } from '@mikro-orm/core'
import { Option } from 'fp-ts/lib/Option'

import { BaseEntity, EntityOption } from '../entity'

@Entity({ tableName: 'posts' })
export class PostEntity extends BaseEntity {
	@Property()
	frozen: boolean = false

	@Property({ nullable: true, type: EntityOption })
	image: Option<string>

	@Property()
	summary: string

	@Property()
	text: string

	@Property()
	title: string
}
