import { pipe } from 'fp-ts/lib/function'

import type { EntityManager, EntityRepository } from '@mikro-orm/postgresql'
import type { PostEntity } from './post/post.entity'

export type Repo = {
	em: EntityManager,
	post: EntityRepository<PostEntity>
}

export const repo: Repo = {} as Repo

export const setRepo = (data: Repo) => pipe(
	data,
	Object.entries,
	entries => entries.forEach(([ key, value ]) => {
		repo[key] = value
	})
)
