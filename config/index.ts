import { tryImport } from './utils.js'

import type { Config } from './config.js'

const env = process.env.ENV || 'production'

const CONFIG: Config = {
	...(require('./config')),
	...(tryImport(`./${env}.config`)),
	env,
}

export const config = {
	get: <A extends keyof Config>(key: A): Config[A] | undefined => CONFIG[key],
	getOrThrow: <A extends keyof Config>(key: A): Config[A] => {
		const value = CONFIG[key]
		if (!value) throw new ConfigError(key)
		return value
	},
}

export class ConfigError extends Error {
	constructor(key: string) {
		super(`${key} not found in configuration.`)
	}
}
