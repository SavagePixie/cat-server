import type { LoggerOptions } from 'pino'
import type { PostgreSqlOptions } from '@mikro-orm/postgresql/PostgreSqlMikroORM'

export type Config = {
	database: PostgreSqlOptions
	env: 'development' | 'production'
	logger: LoggerOptions
	port: number
}

export const database: Config['database'] = {
	dbName: 'test',
	host: 'test',
	password: 'test',
	port: 5432,
	user: 'test',
}

export const logger: Config['logger'] = { level: 'warn' }
