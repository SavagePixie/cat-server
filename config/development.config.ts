import { tryImport } from './utils'

import type { Config } from './config'

const priv = tryImport('./priv.config')

export const database: Config['database'] = {
	debug: true,
	pool: {
		max: 10,
		min: 1,
	},
	...priv.database,
}

export const logger: Config['logger'] = { level: 'debug' }

export const port: Config['port'] = 3000
