import type { Config } from './config'

export const tryImport = (path: string): Partial<Config> => {
	try {
		return require(path)
	} catch {
		return {}
	}
}
