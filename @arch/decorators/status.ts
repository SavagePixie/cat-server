import { STATUS_META } from './internal'

export const status = (statusCode: number) =>
	function (target: any, name: string, _descriptor: PropertyDescriptor) {
		Reflect.defineMetadata(STATUS_META, statusCode, target, name)
	}
