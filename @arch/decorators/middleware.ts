import { MIDDLEWARE_META, MiddlewareMeta } from './internal'

import type { RequestHandler } from 'express'

export const middleware = (handler: RequestHandler) =>
	function (target: any, name: string, _descriptor: PropertyDescriptor) {
		const middlewares: RequestHandler[] = Reflect.getOwnMetadata(
			MIDDLEWARE_META,
			target,
			name
		) ?? []
		Reflect.defineMetadata(
			MIDDLEWARE_META,
			[ ...middlewares, handler ],
			target,
			name
		)
	}

export const use = (path: string | RegExp = '/') =>
	function (
		target: any,
		_name: string,
		descriptor: TypedPropertyDescriptor<() => RequestHandler | RequestHandler[]>
	) {
		const middleware: MiddlewareMeta = Reflect.getMetadata(
			MIDDLEWARE_META,
			target
		) ?? new Map()
		const existing = middleware.get(path) ?? []
		middleware.set(path, [ ...existing, descriptor.value ])
		Reflect.defineMetadata(
			MIDDLEWARE_META,
			middleware,
			target
		)
	}
