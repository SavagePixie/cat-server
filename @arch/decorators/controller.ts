import { CONTROLLER_META } from './internal'

export const controller = (path: string | RegExp) =>
	function(constructor: Function) {
		Reflect.defineMetadata(CONTROLLER_META, path, constructor.prototype)
	}
