import { HEADERS_META, getOwnMetaOr } from './internal'

export const header = (key: string, value: any) =>
	function(target: any, name: string, _descriptor: PropertyDescriptor) {
		const existing = getOwnMetaOr(HEADERS_META, target, name, {})
		Reflect.defineMetadata(
			HEADERS_META,
			{ ...existing, [key]: value },
			target,
			name
		)
	}
