import { Method, ROUTE_META, RouteMeta, STATUS_META } from './internal'

const route = (method: Method, effect?: (target: any, name: string) => void) =>
	(path: string | RegExp = '/') =>
		function (target: any, name: string, descriptor: PropertyDescriptor) {
			const routes: RouteMeta[] = Reflect.getMetadata(ROUTE_META, target) ?? []
			Reflect.defineMetadata(
				ROUTE_META,
				[ ...routes, { handler: descriptor.value, method, name, path }],
				target
			)
			effect?.(target, name)
		}

export const all = route('all')
export const del = route('delete')
export const get = route('get')
export const options = route('options')
export const patch = route('patch')
export const post = route('post', (target: any, name: string) => {
	const statusCode = Reflect.getOwnMetadata(STATUS_META, target, name)
	if (!statusCode) Reflect.defineMetadata(STATUS_META, 201, target, name)
})
export const put = route('put')
