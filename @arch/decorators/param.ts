import { PARAM_META, getOwnMetaOr } from './internal'

import type { Request, Response } from 'express'

export const createExpressParamDecorator =
	(fun: (req: Request, res: Response) => any) =>
		(target: Object, key: string | symbol, index: number) => {
			const params = getOwnMetaOr(PARAM_META, target, key, [])
			params[index] = fun
			Reflect.defineMetadata(PARAM_META, params, target, key)
		}

export const body = createExpressParamDecorator(req => req.body)
export const input = createExpressParamDecorator(req => req.input)
export const params = createExpressParamDecorator(req => req.params)
export const query = createExpressParamDecorator(req => req.query)

export const request = createExpressParamDecorator(req => req)
export const response = createExpressParamDecorator((_, res) => res)

export const fetches =
	(key: string | symbol) =>
		createExpressParamDecorator((_, res) => res.locals.fetches[key])
