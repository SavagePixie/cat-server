import type { Request, RequestHandler, Response } from 'express'

export type HeadersMeta = Record<string, any>

export type Method =
	'all' | 'delete' | 'get' | 'options' | 'patch' | 'post' | 'put'

export type MiddlewareMeta = Map<
	RegExp | string | undefined,
	(() => RequestHandler | RequestHandler[])[]
>

export type ParamMeta = ((req: Request, res: Response) => any)[]

export type RouteHandler = Function

export type RouteMeta = {
	handler: RouteHandler
	method: Method
	name: string | symbol
	path: string
}

export const CONTROLLER_META = Symbol('controller')
export const HEADERS_META = Symbol('headers')
export const MIDDLEWARE_META = Symbol('middleware')
export const PARAM_META = Symbol('parameters')
export const ROUTE_META = Symbol('route')
export const STATUS_META = Symbol('status')

export const getOwnMetaOr =
	<A>(meta: any, target: Object, key: any, def: A): A =>
		Reflect.getOwnMetadata(meta, target, key) ?? def
