import {
	Express, NextFunction,
	Request, Response, Router
} from 'express'
import { P, match } from 'ts-pattern'

import {
	CONTROLLER_META, HEADERS_META, HeadersMeta, MIDDLEWARE_META,
	MiddlewareMeta, PARAM_META, ParamMeta, ROUTE_META,
	RouteHandler, RouteMeta, STATUS_META, getOwnMetaOr
} from './internal'
import { either, option } from 'fp-ts'
import { Logger } from '../modules/logger'
import { M } from '../../src/lib/matchers'

type TypedClass<A extends any[]> = { new (..._deps: A): any }

export const router = <A extends any[]>(
	app: Express,
	controller: TypedClass<A>,
	deps: A
) => {
	const path = Reflect.getOwnMetadata(CONTROLLER_META, controller.prototype)
	if (!path) {
		throw new TypeError('Missing controller path')
	}

	const r = Router()
	const instance = new controller(...deps)

	Logger.info(`Registering controller ${path}`)
	registerMiddlewares(controller, instance, r)
	registerHandlers(controller, instance, r)

	app.use(path, r)
}

const handle = (
	instance: any,
	statusCode: number,
	headers: HeadersMeta,
	params: ParamMeta,
	handler: RouteHandler
) =>
	(req: Request, res: Response, next: NextFunction) => {
		const args = params.map(param => param(req, res))
		return Promise.resolve(handler.apply(instance, args))
			.then(result => {
				if (res.writableEnded) return
				if (either.isLeft(result) || option.isNone(result)) {
					return next(result)
				}

				res.set(headers)

				const data = match(result)
					.when(() => statusCode === 204, () => null)
					.with(P.intersection(M.Right, { right: P.select() }), data => data)
					.with(P.intersection(M.Some, { value: P.select() }), data => data)
					.otherwise(data => data)

				res.status(statusCode).json(data)
			})
			.catch(next)
	}

const registerHandlers =
	(controller: Function, instance: any, router: Router) => {
		const target = controller.prototype
		const handlers = Reflect.getMetadata(ROUTE_META, target) ?? []

		handlers.forEach(({ handler, method, name, path }: RouteMeta) => {
			const headers: HeadersMeta = getOwnMetaOr(HEADERS_META, target, name, {})
			const middlewares = getOwnMetaOr(MIDDLEWARE_META, target, name, [])
			const params: ParamMeta = getOwnMetaOr(PARAM_META, target, name, [])
			const statusCode = getOwnMetaOr(STATUS_META, target, name, 200)

			router[method](
				path,
				...middlewares,
				handle(instance, statusCode, headers, params, handler)
			)
			Logger.info(`--- Registered ${method} ${path}`)
		})
	}

const registerMiddlewares =
	(controller: Function, instance: any, router: Router) => {
		const middlewares: MiddlewareMeta = Reflect.getMetadata(
			MIDDLEWARE_META,
			controller.prototype
		) ?? []
		Array.from(middlewares.entries())
			.forEach(([ path, handlers ]) => router.use(
				path,
				...handlers.flatMap(handler => handler.call(instance))
			))
	}
