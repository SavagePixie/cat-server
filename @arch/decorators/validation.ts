import { array, either } from 'fp-ts'
import { match } from 'ts-pattern'
import { pipe } from 'fp-ts/lib/function'

import { M } from '../../src/lib/matchers'
import { middleware } from './middleware'
import { tap } from '../../src/utils/function'
import { validate } from '../../src/lib/schema'

import type { z } from 'zod'

export const validation =
	(key: 'body' | 'params' | 'query' | 'input', schema: z.ZodTypeAny) =>
		middleware((req, _res, next) => pipe(
			key === 'input'
				? { ...req.query, ...req.body, ...req.params }
				: req[key],
			validate(schema),
			either.mapLeft(array.map(parseValidationIssue)),
			either.bimap(
				message => ({ code: 400, message }),
				tap(parsed => req[key] = parsed)
			),
			value => match(value)
				.with(M.Left, left => next(left))
				.otherwise(() => next())
		))

const parseValidationIssue = (issue: z.ZodIssue) =>
	[ issue.path.join('.'), issue.message ]

