import { P, match } from 'ts-pattern'
import { pipe } from 'fp-ts/lib/function'

import { Logger } from '../modules/logger'
import { M } from '../../src/lib/matchers'

import type { ErrorRequestHandler } from 'express'

const makeErrorResponse = ({ code, message }: ErrorData) => ({
	code,
	data: { message },
})

const INTERNAL_ERROR = () => makeErrorResponse({
	code: 500,
	message: 'internal server error',
})
const NOT_FOUND = () => makeErrorResponse({ code: 404, message: 'not found' })

type ErrorData = {
	code: number
	message: any
}

export const catchError: ErrorRequestHandler = (error, _req, res, _next) => {
	const { code, data } = match(error)
		.with(M.None, NOT_FOUND)
		.with(
			P.intersection(M.Left, { left: P.select({ code: P.number, message: P.any }) }),
			makeErrorResponse
		)
		.otherwise(error => pipe(error, Logger.error, INTERNAL_ERROR))

	res.status(code).json(data)
}
