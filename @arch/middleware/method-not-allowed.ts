import { either } from 'fp-ts'

import type { RequestHandler } from 'express'

export const methodNotAllowed: RequestHandler = (req, res, next) => {
	next(either.left({
		code: 405,
		message: `cannot ${req.method} ${req.originalUrl}`,
	}))
}
