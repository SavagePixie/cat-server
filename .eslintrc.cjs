module.exports = {
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 2023,
		sourceType: 'module',
	},
	extends: [ 'pixie' ],
	ignorePatterns: [ 'dist/**', 'node_modules/**' ],
	plugins: [ '@typescript-eslint' ],
	rules: {
		'arrow-spacing': 'error',
		'indent': [ 'error', 'tab', { 'ignoredNodes': [ 'PropertyDefinition' ] }],
		'max-len': [ 'error', {
			code: 80,
			ignoreComments: true,
			ignorePattern: '^\\s*import(\\stype)?\\s\\{\\s\\w+\\s\\}',
			ignoreRegExpLiterals: true,
			ignoreTemplateLiterals: true,
			tabWidth: 0, // Ignores tabs when calculating line width
		}],
		'newline-per-chained-call': [ 'error' ],
		'no-multiple-empty-lines': [ 'error', { max: 1, maxBOF: 0, maxEOF: 1 }],
		'no-unused-vars': 'off',
		'object-curly-newline': [ 'error', { multiline: true }],
		'object-curly-spacing': [ 'error', 'always' ],
		'object-property-newline': [ 'error', { allowAllPropertiesOnSameLine: true }],
		'quotes': [ 'error', 'single', { allowTemplateLiterals: true }],
		'sort-imports': [ 'error', { allowSeparatedGroups: true }],
		'space-infix-ops': 'error',
		'@typescript-eslint/no-unused-vars': [ 'error', {
			args: 'after-used',
			'argsIgnorePattern': '^_',
	  }],
	},
}
